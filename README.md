# FFVFollowersCountFormatter

[![CI Status](http://img.shields.io/travis/Filip Beć/FFVFollowersCountFormatter.svg?style=flat)](https://travis-ci.org/Filip Beć/FFVFollowersCountFormatter)
[![Version](https://img.shields.io/cocoapods/v/FFVFollowersCountFormatter.svg?style=flat)](http://cocoadocs.org/docsets/FFVFollowersCountFormatter)
[![License](https://img.shields.io/cocoapods/l/FFVFollowersCountFormatter.svg?style=flat)](http://cocoadocs.org/docsets/FFVFollowersCountFormatter)
[![Platform](https://img.shields.io/cocoapods/p/FFVFollowersCountFormatter.svg?style=flat)](http://cocoadocs.org/docsets/FFVFollowersCountFormatter)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FFVFollowersCountFormatter is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "FFVFollowersCountFormatter"

## Author

Filip Beć, filip.bec@infinum.hr

## License

FFVFollowersCountFormatter is available under the MIT license. See the LICENSE file for more info.


Pod::Spec.new do |s|
  s.name             = "FFVFollowersCountFormatter"
  s.version          = "0.1.0"
  s.summary          = "Followers count formatter"
  s.description      = <<-DESC
                       Shorten number to thousands and millions.
                       DESC
  s.homepage         = "https://bitbucket.org/filipbec/ffvfollowerscountformatter"
  s.license          = 'MIT'
  s.author           = { "Filip Beć" => "filip.bec@infinum.hr" }
  s.source           = { :git => "https://filipbec@bitbucket.org/filipbec/ffvfollowerscountformatter.git", :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/infinum_hr'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
end

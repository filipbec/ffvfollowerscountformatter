//
//  FFVFollowersCountFormatterTests.m
//  FFVFollowersCountFormatterTests
//
//  Created by Filip Beć on 03/15/2015.
//  Copyright (c) 2014 Filip Beć. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

#import "FFVFollowersCountFormatter.h"

@interface FFVFollowersCountFormatterTests : XCTestCase

@property (nonatomic, strong) FFVFollowersCountFormatter *formatter;

@end

@implementation FFVFollowersCountFormatterTests

- (void)setUp {
    [super setUp];
    self.formatter = [[FFVFollowersCountFormatter alloc] init];
    [self.formatter setDecimalSeparator:@","];
    [self.formatter setGroupingSeparator:@"."];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)test_1
{
    NSString *string = [self.formatter stringFromNumber:@(1)];
    XCTAssert([string isEqualToString:@"1"]);
}

- (void)test_100
{
    NSString *string = [self.formatter stringFromNumber:@(100)];
    XCTAssert([string isEqualToString:@"100"]);
}

- (void)test_999
{
    NSString *string = [self.formatter stringFromNumber:@(999)];
    XCTAssert([string isEqualToString:@"999"]);
}

- (void)test_1_000
{
    NSString *string = [self.formatter stringFromNumber:@(1000)];
    XCTAssert([string isEqualToString:@"1.000"]);
}

- (void)test_5_555
{
    NSString *string = [self.formatter stringFromNumber:@(5555)];
    XCTAssert([string isEqualToString:@"5.555"]);
}

- (void)test_9_999
{
    NSString *string = [self.formatter stringFromNumber:@(9999)];
    XCTAssert([string isEqualToString:@"9.999"]);
}

- (void)test_10_000
{
    NSString *string = [self.formatter stringFromNumber:@(10000)];
    XCTAssert([string isEqualToString:@"10K"]);
}

- (void)test_99_999
{
    NSString *string = [self.formatter stringFromNumber:@(99999)];
    XCTAssert([string isEqualToString:@"99,9K"]);
}

- (void)test_100_000
{
    NSString *string = [self.formatter stringFromNumber:@(100000)];
    XCTAssert([string isEqualToString:@"100K"]);
}

- (void)test_999_999
{
    NSString *string = [self.formatter stringFromNumber:@(999999)];
    XCTAssert([string isEqualToString:@"999,9K"]);
}

- (void)test_1_000_000
{
    NSString *string = [self.formatter stringFromNumber:@(1000000)];
    XCTAssert([string isEqualToString:@"1M"]);
}

- (void)test_23_456_789
{
    NSString *string = [self.formatter stringFromNumber:@(23456789)];
    XCTAssert([string isEqualToString:@"23,4M"]);
}

- (void)testDecimalSeparator
{
    FFVFollowersCountFormatter *formatter = [FFVFollowersCountFormatter new];
    formatter.decimalSeparator = @"|";
    
    NSString *string = [formatter stringFromNumber:@(123456)];
    XCTAssert([string isEqualToString:@"123|4K"]);
}

- (void)testGroupingSeparator
{
    FFVFollowersCountFormatter *formatter = [FFVFollowersCountFormatter new];
    formatter.groupingSeparator = @"|";
    
    NSString *string = [formatter stringFromNumber:@(1234)];
    XCTAssert([string isEqualToString:@"1|234"]);
}

- (void)testUsesGroupingSeparator
{
    FFVFollowersCountFormatter *formatter = [FFVFollowersCountFormatter new];
    formatter.groupingSeparator = @"|";
    
    formatter.usesGroupingSeparator = YES;
    NSString *string1 = [formatter stringFromNumber:@(1234)];
    XCTAssert([string1 isEqualToString:@"1|234"]);
    
    
    formatter.usesGroupingSeparator = NO;
    NSString *string2 = [formatter stringFromNumber:@(1234)];
    XCTAssert([string2 isEqualToString:@"1234"]);
}

- (void)testMaximumFractionDigits
{
    FFVFollowersCountFormatter *formatter = [FFVFollowersCountFormatter new];
    formatter.decimalSeparator = @",";
    formatter.maximumFractionDigits = 3;
    
    NSString *string = [formatter stringFromNumber:@(123456)];
    XCTAssert([string isEqualToString:@"123,456K"]);
}

@end

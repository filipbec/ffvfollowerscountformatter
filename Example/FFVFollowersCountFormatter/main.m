//
//  main.m
//  FFVFollowersCountFormatter
//
//  Created by Filip Beć on 03/15/2015.
//  Copyright (c) 2014 Filip Beć. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FFVAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FFVAppDelegate class]));
    }
}

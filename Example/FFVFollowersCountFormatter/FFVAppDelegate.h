//
//  FFVAppDelegate.h
//  FFVFollowersCountFormatter
//
//  Created by CocoaPods on 03/15/2015.
//  Copyright (c) 2014 Filip Beć. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FFVAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

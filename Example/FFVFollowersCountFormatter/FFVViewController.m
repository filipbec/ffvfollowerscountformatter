//
//  FFVViewController.m
//  FFVFollowersCountFormatter
//
//  Created by Filip Beć on 03/15/2015.
//  Copyright (c) 2014 Filip Beć. All rights reserved.
//

#import "FFVViewController.h"
#import <FFVFollowersCountFormatter/FFVFollowersCountFormatter.h>

@interface FFVViewController ()

@property (weak, nonatomic) IBOutlet UILabel *followersCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *followingCountLabel;
@end

@implementation FFVViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    FFVFollowersCountFormatter *formatter = [[FFVFollowersCountFormatter alloc] init];
    self.followersCountLabel.text = [formatter stringFromNumber:@(12345678)];
    self.followingCountLabel.text = [formatter stringFromNumber:@(654321)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

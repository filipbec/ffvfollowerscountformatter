//
//  FFVFollowersCountFormatter.m
//  Foodfave
//
//  Created by Filip Beć on 10/03/15.
//  Copyright (c) 2015 Infinum Ltd. All rights reserved.
//

#import "FFVFollowersCountFormatter.h"

@interface FFVFollowersCountFormatter ()

@property (nonatomic, strong) NSNumberFormatter *formatter;

@end

@implementation FFVFollowersCountFormatter

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.formatter = [[NSNumberFormatter alloc] init];
        self.formatter.usesGroupingSeparator = YES;
        self.formatter.maximumFractionDigits = 1;
        self.formatter.roundingMode = NSNumberFormatterRoundFloor;
    }
    return self;
}

- (NSString *)stringForObjectValue:(id)obj
{
    NSParameterAssert([obj isKindOfClass:[NSNumber class]]);
    return [self stringFromNumber:obj];
}

- (NSString *)stringFromNumber:(NSNumber *)number
{
    float count = [number integerValue];
    if (count > 0) {
        if (count < 10000) {
            return [self.formatter stringFromNumber:@(count)];
        } else if (count < 1000000) {
            count /= 1000.0;
            return [NSString stringWithFormat:@"%@K", [self.formatter stringFromNumber:@(count)]];
        } else {
            count /= 1000000;
            return [NSString stringWithFormat:@"%@M", [self.formatter stringFromNumber:@(count)]];
        }
    }
    
    return @"0";
}

// forward methods from NumberFormatter category to formatter instance
- (id)forwardingTargetForSelector:(SEL)aSelector
{
    return self.formatter;
}

@end

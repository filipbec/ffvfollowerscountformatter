//
//  FFVFollowersCountFormatter.h
//  Foodfave
//
//  Created by Filip Beć on 10/03/15.
//  Copyright (c) 2015 Infinum Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FFVFollowersCountFormatter : NSFormatter

/**
 'obj' must be an instance of NSNumber.
 */
- (NSString *)stringForObjectValue:(id)obj;

- (NSString *)stringFromNumber:(NSNumber *)number;

@end

@interface FFVFollowersCountFormatter (NumberFormatter)

@property (copy) NSLocale *locale;
@property (copy) NSString *decimalSeparator;
@property (copy) NSString *groupingSeparator;

/// Default is @c YES.
@property BOOL usesGroupingSeparator;

/// Maximum number of decimal digits. Default is @c 1.
@property NSUInteger maximumFractionDigits;

@end